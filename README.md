# Funciones en JavaScript

El propósito de este repositorio es tener evidencia de lo aprendido en la clase respecto a funciones.

## Metodología

Para el uso de funciones, lo que se ha hecho es crear un objeto JSON llamado *funciones* en el cual se han creado las distintas funciones (suma, resta, multiplicación, divisón y módulo).

Si deseas ver el código, ingresa en: 

[Código JavaScript](https://gitlab.com/grupo-matutino/clase-javascript/blob/master/clase-javascript/funciones.js)
