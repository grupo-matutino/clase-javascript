"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var clase_usuarios_1 = require("./clase-usuarios");
var clase_usuarios_2 = require("./clase-usuarios");
var estudiantePoli = /** @class */ (function (_super) {
    __extends(estudiantePoli, _super);
    function estudiantePoli(nombre, apellido, carrera, numeroUnico) {
        var _this = _super.call(this, nombre, apellido) || this;
        _this.nombre = nombre;
        _this.apellido = apellido;
        _this.carrera = carrera;
        _this.numeroUnico = numeroUnico;
        _this.carrera = carrera;
        _this.numeroUnico = numeroUnico;
        return _this;
    }
    return estudiantePoli;
}(clase_usuarios_1.usuarioBase));
exports.estudiantePoli = estudiantePoli;
var estudiantePoliConGetYSet = /** @class */ (function (_super) {
    __extends(estudiantePoliConGetYSet, _super);
    function estudiantePoliConGetYSet(nombre, apellido, carrera, numeroUnico) {
        var _this = _super.call(this, nombre, apellido) || this;
        _this.nombre = nombre;
        _this.apellido = apellido;
        _this.carrera = carrera;
        _this.numeroUnico = numeroUnico;
        _this.carrera = carrera;
        _this.numeroUnico = numeroUnico;
        return _this;
    }
    return estudiantePoliConGetYSet;
}(clase_usuarios_2.usuarioBaseConGetYSet));
exports.estudiantePoliConGetYSet = estudiantePoliConGetYSet;
