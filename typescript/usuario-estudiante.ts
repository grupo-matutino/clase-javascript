import{ usuarioBase } from './clase-usuarios'
import{ usuarioBaseConGetYSet } from './clase-usuarios'

export class estudiantePoli extends usuarioBase
{
    constructor(public nombre:string, public apellido:string, public carrera:string, public numeroUnico:number)
    {
        super(nombre, apellido)
        this.carrera = carrera
        this.numeroUnico = numeroUnico
    }
}

export class estudiantePoliConGetYSet extends usuarioBaseConGetYSet
{
    constructor(public nombre:string, public apellido:string, public carrera:string, public numeroUnico:number)
    {
        super(nombre, apellido)
        this.carrera = carrera
        this.numeroUnico = numeroUnico
    }
}