"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var clase_usuarios_1 = require("./clase-usuarios");
var clase_usuarios_2 = require("./clase-usuarios");
var empleadoSupermaxi = /** @class */ (function (_super) {
    __extends(empleadoSupermaxi, _super);
    function empleadoSupermaxi(nombre, apellido, cargo) {
        var _this = _super.call(this, nombre, apellido) || this;
        _this.nombre = nombre;
        _this.apellido = apellido;
        _this.cargo = cargo;
        _this.cargo = cargo;
        return _this;
    }
    return empleadoSupermaxi;
}(clase_usuarios_1.usuarioBase));
exports.empleadoSupermaxi = empleadoSupermaxi;
var empleadoSupermaxiConGetYSet = /** @class */ (function (_super) {
    __extends(empleadoSupermaxiConGetYSet, _super);
    function empleadoSupermaxiConGetYSet(nombre, apellido, cargo) {
        var _this = _super.call(this, nombre, apellido) || this;
        _this.nombre = nombre;
        _this.apellido = apellido;
        _this.cargo = cargo;
        _this.cargo = cargo;
        return _this;
    }
    return empleadoSupermaxiConGetYSet;
}(clase_usuarios_2.usuarioBaseConGetYSet));
exports.empleadoSupermaxiConGetYSet = empleadoSupermaxiConGetYSet;
