const edad:number = Math.floor(Math.random()*(18-50))

export class usuarioBaseConGetYSet
{
    //Las variables privadas siempre se escriben con un guión bajo al comienzo del nombre de la variable
    private _nombre:string;
    private _apellido:string;
    public edad:number;

    constructor(nombre:string, apellido:string)
    {
        this._nombre = nombre;
        this._apellido = apellido;
        this.edad = edad;
    }
    
    get nombre()
    {
        return this._nombre
    }

    set nombre(nombre:string)
    {
        this._nombre = nombre
    }

    get apellido()
    {
        return this._apellido
    }

    set apellido(apellido:string)
    {
        this._apellido = apellido
    }
}

export class usuarioBase
{
    public edad

    constructor(protected nombre:string, protected apellido:string)
    {
        this.nombre = nombre
        this.apellido = apellido
        this.edad = edad;
    }
}