import{ estudiantePoli } from './usuario-estudiante'
import{ estudiantePoliConGetYSet } from './usuario-estudiante'
import{ empleadoSupermaxi } from './empleado-supermaxi'
import{ empleadoSupermaxiConGetYSet } from './empleado-supermaxi'
import{ usuarioPolicia } from './usuario-policia'
import{ usuarioPoliciaConGetYSet } from './usuario-policia'

//Creacion de nuevos estudiantes 
console.log('CREACION DE ESTUDIANTES')

const nuevoEstudiante = new estudiantePoli('Andrés', 'Torres', 'Sistemas', 20142056)
console.log('Estudiante: ', nuevoEstudiante)

const otroEstudiante = new estudiantePoliConGetYSet('Mateo', 'Narváez', 'Mecánica', 20142258)
console.log('Estudiante con get y set: ', otroEstudiante)

//Creacion de empleados del supermaxi
console.log('CREACION DE EMPLEADOS DEL SUPERMAXI')

const nuevoEmpleado = new empleadoSupermaxi('Lalo', 'Landa', 'cajero')
console.log('Empleado de supermaxi: ', nuevoEmpleado)

const otroEmpleado = new empleadoSupermaxiConGetYSet('Rita', 'Laso', 'bodeguera')
console.log('Empleado con get y set: ', otroEmpleado)

//Creacion de miembros del cuerpo policial
console.log('CREACION DE NUEVOS MIEMBROS DEL EQUIPO POLICIAL')

const nuevoPolicia = new usuarioPolicia('Paco', 'Moncayo', 'coronel')
console.log('Miembro del cuerpo policial: ', nuevoPolicia)

const otroPolicia = new usuarioPoliciaConGetYSet('Augustus', 'Sinclair', 'policía')
console.log('Miembro del cuerpo policial con get y set: ', otroPolicia)
