import{ usuarioBase } from './clase-usuarios'
import{ usuarioBaseConGetYSet } from './clase-usuarios'

export class empleadoSupermaxi extends usuarioBase
{
    constructor(public nombre:string, public apellido:string, public cargo:string)
    {
        super(nombre, apellido)
        this.cargo = cargo
    }
}

export class empleadoSupermaxiConGetYSet extends usuarioBaseConGetYSet
{
    constructor(public nombre:string, public apellido:string, public cargo:string)
    {
        super(nombre, apellido)
        this.cargo = cargo
    }
}
