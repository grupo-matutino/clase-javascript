"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var usuario_estudiante_1 = require("./usuario-estudiante");
var usuario_estudiante_2 = require("./usuario-estudiante");
var empleado_supermaxi_1 = require("./empleado-supermaxi");
var empleado_supermaxi_2 = require("./empleado-supermaxi");
var usuario_policia_1 = require("./usuario-policia");
var usuario_policia_2 = require("./usuario-policia");
//Creacion de nuevos estudiantes 
console.log('CREACION DE ESTUDIANTES');
var nuevoEstudiante = new usuario_estudiante_1.estudiantePoli('Andrés', 'Torres', 'Sistemas', 20142056);
console.log('Estudiante: ', nuevoEstudiante);
var otroEstudiante = new usuario_estudiante_2.estudiantePoliConGetYSet('Mateo', 'Narváez', 'Mecánica', 20142258);
console.log('Estudiante con get y set: ', otroEstudiante);
//Creacion de empleados del supermaxi
console.log('CREACION DE EMPLEADOS DEL SUPERMAXI');
var nuevoEmpleado = new empleado_supermaxi_1.empleadoSupermaxi('Lalo', 'Landa', 'cajero');
console.log('Empleado de supermaxi: ', nuevoEmpleado);
var otroEmpleado = new empleado_supermaxi_2.empleadoSupermaxiConGetYSet('Rita', 'Laso', 'bodeguera');
console.log('Empleado con get y set: ', otroEmpleado);
//Creacion de miembros del cuerpo policial
console.log('CREACION DE NUEVOS MIEMBROS DEL EQUIPO POLICIAL');
var nuevoPolicia = new usuario_policia_1.usuarioPolicia('Paco', 'Moncayo', 'coronel');
console.log('Miembro del cuerpo policial: ', nuevoPolicia);
var otroPolicia = new usuario_policia_2.usuarioPoliciaConGetYSet('Augustus', 'Sinclair', 'policía');
console.log('Miembro del cuerpo policial con get y set: ', otroPolicia);
