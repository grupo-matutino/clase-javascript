import{ usuarioBase } from './clase-usuarios'
import{ usuarioBaseConGetYSet } from './clase-usuarios'

export class usuarioPolicia extends usuarioBase
{
    constructor(public nombre:string, public apellido:string, public grado:string)
    {
        super(nombre, apellido)
        this.grado = grado
    }
}

export class usuarioPoliciaConGetYSet extends usuarioBaseConGetYSet
{
    constructor(public nombre:string, public apellido:string, public grado:string)
    {
        super(nombre, apellido)
        this.grado = grado
    }
}