"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var edad = Math.floor(Math.random() * (18 - 50));
var usuarioBaseConGetYSet = /** @class */ (function () {
    function usuarioBaseConGetYSet(nombre, apellido) {
        this._nombre = nombre;
        this._apellido = apellido;
        this.edad = edad;
    }
    Object.defineProperty(usuarioBaseConGetYSet.prototype, "nombre", {
        get: function () {
            return this._nombre;
        },
        set: function (nombre) {
            this._nombre = nombre;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(usuarioBaseConGetYSet.prototype, "apellido", {
        get: function () {
            return this._apellido;
        },
        set: function (apellido) {
            this._apellido = apellido;
        },
        enumerable: true,
        configurable: true
    });
    return usuarioBaseConGetYSet;
}());
exports.usuarioBaseConGetYSet = usuarioBaseConGetYSet;
var usuarioBase = /** @class */ (function () {
    function usuarioBase(nombre, apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
    return usuarioBase;
}());
exports.usuarioBase = usuarioBase;
