"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var clase_usuarios_1 = require("./clase-usuarios");
var clase_usuarios_2 = require("./clase-usuarios");
var usuarioPolicia = /** @class */ (function (_super) {
    __extends(usuarioPolicia, _super);
    function usuarioPolicia(nombre, apellido, grado) {
        var _this = _super.call(this, nombre, apellido) || this;
        _this.nombre = nombre;
        _this.apellido = apellido;
        _this.grado = grado;
        _this.grado = grado;
        return _this;
    }
    return usuarioPolicia;
}(clase_usuarios_1.usuarioBase));
exports.usuarioPolicia = usuarioPolicia;
var usuarioPoliciaConGetYSet = /** @class */ (function (_super) {
    __extends(usuarioPoliciaConGetYSet, _super);
    function usuarioPoliciaConGetYSet(nombre, apellido, grado) {
        var _this = _super.call(this, nombre, apellido) || this;
        _this.nombre = nombre;
        _this.apellido = apellido;
        _this.grado = grado;
        _this.grado = grado;
        return _this;
    }
    return usuarioPoliciaConGetYSet;
}(clase_usuarios_2.usuarioBaseConGetYSet));
exports.usuarioPoliciaConGetYSet = usuarioPoliciaConGetYSet;
