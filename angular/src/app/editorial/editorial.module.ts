import { NgModule } from "@angular/core";
import { EditorialInfoComponet } from "./editorial-info/editorial.component";
import { EditorialCrearComponent } from "./editorial-crear/editorial-crear.component";
import { ModuloComun } from "../modulo-comun/modulo-comun.module";

@NgModule({
    declarations: [
        EditorialInfoComponet,
        EditorialCrearComponent,
        
    ],
    imports: [ModuloComun],
    exports: [
        EditorialInfoComponet,
        EditorialCrearComponent
    ],
    providers: []
})
export class EditorialModule { }