import { NgModule } from "@angular/core";
import { InfoAutorComponent } from "../autor/info-autor/autor.component";

@NgModule({
    declarations:[
        InfoAutorComponent,
    ],
    imports:[],
    providers:[],
    exports:[
        InfoAutorComponent
    ]
})
export class ModuloComun{

}