import { Route, RouterModule } from "@angular/router";
import { LibroCrearComponent } from "./libro/libro-crear/libro-crear.component";
import { InfoLibroComponent } from "./libro/libro-info/libro.component";
import { RutaNoEncontrada } from "./ruta-no-encontrada/ruta-no-encontrada.component";
import { NgModule } from "@angular/core";
import { AutorListarComponent } from "./autor/autor-listar/autor-listar.component";
import { CrearAutorComponent } from "./autor/crear-autor/crear-autor.component";
import { InfoAutorComponent } from "./autor/info-autor/autor.component";
import { EditorialCrearComponent } from "./editorial/editorial-crear/editorial-crear.component";
import { EditorialInfoComponet } from "./editorial/editorial-info/editorial.component";
import { InicioComponent } from "./inicio/inicio.component";

const rutas: Route[] = [
    {
        path: 'inicio',
        component: InicioComponent,
    },
    {
        path: 'crear-libro',
        component: LibroCrearComponent
    },
    {
        path: 'listar-autor',
        component: AutorListarComponent
    },
    {
        path: 'crear-autor',
        component: CrearAutorComponent
    },
    {
        path: 'info-autor',
        component: InfoAutorComponent
    },
    {
        path: 'crear-editorial',
        component: EditorialCrearComponent
    },
    {
        path: 'info-editorial',
        component: EditorialInfoComponet
    },
    {
        path: 'crear-libro',
        component: LibroCrearComponent
    },
    {
        path: 'info-libro',
        component: InfoLibroComponent
    },
    {
        path: '',
        redirectTo: 'inicio',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: RutaNoEncontrada
    },

]


@NgModule({
    imports: [RouterModule.forRoot(rutas, { useHash: true })],
    exports: [RouterModule]

})

export class RutasModule {

}

