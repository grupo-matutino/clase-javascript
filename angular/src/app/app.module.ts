import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { InfoLibroComponent } from './libro/libro-info/libro.component';
import { LibroCrearComponent } from './libro/libro-crear/libro-crear.component';
import { AutorModule } from './autor/autor.module';
import { EditorialModule } from './editorial/editorial.module';
import { ModuloComun } from './modulo-comun/modulo-comun.module';
import { RouterModule } from '@angular/router'
import { RutaNoEncontrada } from './ruta-no-encontrada/ruta-no-encontrada.component';
import { RutasModule } from './app.routes';
import { InicioComponent } from './inicio/inicio.component';

@NgModule({
  declarations: [
    AppComponent,
    InfoLibroComponent,
    LibroCrearComponent,
    RutaNoEncontrada,
    InicioComponent

  ],
  imports: [
    BrowserModule,
    AutorModule,
    EditorialModule,
    ModuloComun,
    RutasModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
