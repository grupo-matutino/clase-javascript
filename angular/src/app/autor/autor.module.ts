import { NgModule } from "@angular/core";
import { AUTOR_DECLARATION } from "./autor-declaration";
import { AutorListarComponent } from "./autor-listar/autor-listar.component";
import { CrearAutorComponent } from "./crear-autor/crear-autor.component";
import { InfoAutorComponent } from "./info-autor/autor.component";
import { ModuloComun } from "../modulo-comun/modulo-comun.module";

@NgModule({
    declarations: [
        ...AUTOR_DECLARATION
    ],
    imports: [ModuloComun],
    exports: [
        AutorListarComponent,
        CrearAutorComponent,
    ],
    providers: []
})
export class AutorModule { }