import { AutorListarComponent } from "./autor-listar/autor-listar.component";

import { CrearAutorComponent } from "./crear-autor/crear-autor.component";

import { InfoAutorComponent } from "./info-autor/autor.component";

export const AUTOR_DECLARATION = [
    AutorListarComponent,
    CrearAutorComponent,
]