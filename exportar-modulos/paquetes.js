const sumar = require('./suma.js');
const restar = require('./resta.js');
const multiplicar = require('./multiplicacion.js')

module.exports = {
    sumar,
    restar,
    multiplicar
}