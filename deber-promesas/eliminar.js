module.exports = (arregloUsuarios, indiceUsuarioAEliminar) => {

	return new Promise((resolve, reject) => {

		arregloUsuarios.splice(indiceUsuarioAEliminar, 1)

		resolve({
			Mensaje: 'Se ha eliminado el usuario exitosamente',
			arregloUsuarios: arregloUsuarios
		})
	})  
} 