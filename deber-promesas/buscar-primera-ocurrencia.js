module.exports = (arregloUsuarios, usuarioABuscar) => {

    return new Promise((resolve, reject) => {

        const usuarioEncontrado = arregloUsuarios.find((usuario) => {

            if((usuarioABuscar.Nombre === usuario.Nombre) && (usuarioABuscar.Apellido === usuario.Apellido))
            {
                return usuario
            }
        })

        const indiceUsuario = arregloUsuarios.findIndex((usuario) => {

            if((usuarioABuscar.Nombre === usuario.Nombre) && (usuarioABuscar.Apellido === usuario.Apellido))
            {
                return usuario
            }
        })

        if(usuarioEncontrado)
        {

            resolve({
                Mensaje: 'Se ha encontrado al usuario',
                usuarioBuscado: usuarioABuscar,
                Posicion: indiceUsuario
            })
        }

        else
        {
            reject({
                Mensaje: 'El usuario que busca no existe',
                usuarioBuscado: usuarioABuscar
            })
        }
    })
}