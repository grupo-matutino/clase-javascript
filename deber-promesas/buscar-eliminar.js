const buscarUsuario = require('./buscar-primera-ocurrencia.js')
const eliminarUsuario = require('./eliminar.js')

module.exports = (arregloUsuarios, usuarioAEliminar) => {

	return new Promise((resolve, reject) => {

		buscarUsuario(arregloUsuarios, usuarioAEliminar, (resultadoBusqueda) => {

			if(resultadoBusqueda.Mensaje == 'Se ha encontrado al usuario')
			{

				eliminarUsuario(arregloUsuarios, resultadoBusqueda.Posicion, (resultadoEliminacion) => {
				
					resolve({

						Mensaje: 'El usuario ha sido eliminado',
					})
				})
			}

			else
			{
				reject({

					Mensaje: 'El usuario que desea eliminar no existe',

				})
			}
		})
	})
}