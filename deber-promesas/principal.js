const metodos = require('./metodos.js')

const arregloUsuarios = [
	{ Nombre: 'Abigail', Apellido: 'Carvajal' },
	{ Nombre: 'Carlos', Apellido: 'Morales' },
	{ Nombre: 'Beatriz', Apellido: 'Reyes' },
	{ Nombre: 'Briggitte', Apellido: 'Villegas' },
	{ Nombre: 'Ernesto', Apellido: 'Yaselga' }
]

const nuevoUsuario = { Nombre: 'Beatriz', Apellido: 'Reyes' }

metodos.buscarYCrear(arregloUsuarios, nuevoUsuario).then((respuesta)  => {
	console.log(respuesta)
}).catch((errorPromesa) => {
	console.log(errorPromesa)
})