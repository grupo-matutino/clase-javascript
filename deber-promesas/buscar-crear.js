const buscarUsuario = require('./buscar-primera-ocurrencia.js')
const crearUsuario = require('./crear.js')

module.exports = (arregloUsuarios, nuevoUsuario) => {

    return new Promise((resolve, reject) => {

        buscarUsuario(arregloUsuarios, nuevoUsuario, (resultadoBusqueda) => {

            if(resultadoBusqueda.Mensaje == 'Se ha encontrado al usuario')
            {

                resolve({
                    Mensaje: 'Se ha encontrado el usuario',
                    Usuario: resultadoBusqueda.usuarioBuscado
                })
            }

            else
            {
                crearUsuario(arregloUsuarios, nuevoUsuario, (resultadoUsuarioCreado) => {

                    reject({
                      Mensaje: 'El usuario no existía, se ha agregado al nuevo usuario',
                        Usuario: resultadoUsuarioCreado.arregloUsuarios
                    })
                })
            }
        })
    })
}
