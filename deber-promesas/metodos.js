const buscarUsuario = require('./buscar-primera-ocurrencia.js')
const crearNuevoUsuario = require('./crear.js')
const buscarYCrear = require('./buscar-crear.js')
const eliminarUsuario = require('./buscar-eliminar.js')

module.exports = {
    buscarUsuario,
    crearNuevoUsuario,
    buscarYCrear,
    eliminarUsuario
}