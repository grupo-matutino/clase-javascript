const menu = require('console-menu')
const lectorConsola = require('readline-sync')

console.log('Bienvenido, seleccione una opcion:')

//const arregloUsuarios = []
const arregloUsuarios = ['Andres', 'Alexander', 'Gorky']
const arregloArtistas = ['Queen', 'The Kooks', 'The Strokes']

var arregloAlbumnesTheKooks = [
	
	{
		Nombre: 'Inside In/Inside Out',
		Canciones: 
		{
			track1: 'Seaside',
			track2: 'Sofa Song',
			track3: 'Naive'
		}
	},

	{
		Nombre: 'Konk',
		Canciones: 
		{
			track1: 'See the Sun',
			track2: 'Gap',
			track3: 'Sway'			
		}
	}  
]

var arregloAlbumnesTheStrokes = [
	
	{
		Nombre: 'Comedown Machine',
		Canciones: 
		{
			track1: 'Tap Out',
			track2: '80s Comedown Machine',
			track3: 'Chances'
		}
	}, 

	{
		Nombre: 'Future Present Past',
		Canciones: 
		{
			track1: 'Drag Queen',
			track2: 'Threat of Joy',
			track3: 'OBLIVIOUS'
		}
	}
] 

var arregloAlbumnesQueen = [
	
	{
		Nombre: 'A Night at the Opera',
		Canciones: 
		{
			track1: '39',
			track2: 'Love of My Life',
			track3: 'Bohemian Rhapsody'
		}
	},

	{
		Nombre: 'News of the World',
		Canciones: 
		{
			track1: 'We Will Rock You',
			track2: 'All Dead, All Dead',
			track3: 'My Melancholy Blues'			
		}
	} 
]

//console.log(arregloArtistas.theKooks[1].Album.Canciones.track3)

//Funciones para el Menu Principal 
const registrarUsuarios = (usuario) => {

	var nuevoUsuario = lectorConsola.question('Ingrese su nombre: ')

	var ingresoExitoso = false

	if(usuario.length > 0)
	{
		var controladorUsuarios = usuario.forEach((valor) => {

			if(valor == nuevoUsuario)
			{
				console.log('El usuario que desea ingresar ya existe, por favor, intente nuevamente')
				ejecutarMenuPrincipal()
			}

			else
			{
				ingresoExitoso = true
			}		
		})
	}

	if(ingresoExitoso = true)
	{
		usuario.push(nuevoUsuario)
		console.log('Usuario ingresado exitosamente')
	}

	ejecutarMenuPrincipal()
}

const logearUsuarios = (usuario) => {

	try
	{
		controladorMenuLogging(usuario)

		indiceUsuario = lectorConsola.keyInSelect(arregloUsuarios, '¿Quién eres?')

		var usuarioALoggearse = indiceUsuario + 1

		if(usuarioALoggearse > 0)
		{
			console.log(`Bienvenido ${usuario[indiceUsuario]}, accediendo al menú de artistas`)
			ejecutarMenuArtistas()
		} 

		else 
		{
			console.log('Usted seleccionó CANCEL, volviendo al menú principal')
			ejecutarMenuPrincipal()
		}
	}

	catch(err)
	{
		console.log('No existen usuarios')

		ejecutarMenuPrincipal()
	}
} 

const controladorMenuLogging = (usuarios) => {

	const hayUsuarios = usuarios.length > 0

	if(hayUsuarios)
	{
		console.log('Ingresando al loggin')
	}

	else 
	{
		console.log('No existen usuarios')
		ejecutarMenuPrincipal()
	}
}

//Funciones para el Menu Artistas
const listarArtistasDisponibles = (artistas) => {

	indiceArtistas = lectorConsola.keyInSelect(artistas, '¿Qué banda deseas?')

	var albumAMostrarse = indiceArtistas + 1

	var i = 0

	if(albumAMostrarse === 0)
	{
		console.log('Usted seleccionó CANCEL, volviendo al menú de artistas')
		ejecutarMenuArtistas()
	}

	else
	{
		console.log(`Usted seleccionó a ${artistas[indiceArtistas]}, sus álbumnes son:`)
	}


	while(i < arregloAlbumnesQueen.length || i < arregloAlbumnesTheKooks.length || i < arregloAlbumnesTheStrokes.length)
	{
		if(albumAMostrarse == 1)
		{
			console.log(arregloAlbumnesQueen[i].Nombre)
			i++
		}

		else if(albumAMostrarse == 2)
		{
			console.log(arregloAlbumnesTheKooks[i].Nombre)
			i++
		}

		else if(albumAMostrarse == 3)
		{
			console.log(arregloAlbumnesTheStrokes[i].Nombre)
			i++
		}
	}

	ejecutarMenuArtistas()
}

function agregarAlbum(arregloAlbumnes)
{
	nuevoAlbum = lectorConsola.question('Escriba el nombre del álbum a agregar: ')
	arregloAlbumnes.push({ Nombre: nuevoAlbum })

	return arregloAlbumnes
}

const listarAlbumnesAgregados = (artistas) => {

	indiceArtistas = lectorConsola.keyInSelect(artistas, '¿A qué banda deseas agregar un nuevo álbum?')
	console.log(`Usted seleccionó a ${artistas[indiceArtistas]}`)

	var albumAMostrarse = indiceArtistas + 1

	if(albumAMostrarse == 1)
	{			
		agregarAlbum(arregloAlbumnesQueen)
		console.log(`Se ha agregado un nuevo álbum para ${artistas[indiceArtistas]}`)
		console.log(arregloAlbumnesQueen)		
	}

	else if(albumAMostrarse == 2)
	{
		agregarAlbum(arregloAlbumnesTheKooks)
		console.log(`Se ha agregado un nuevo álbum para ${artistas[indiceArtistas]}`)	
		console.log(arregloAlbumnesTheKooks)
	}

	else if(albumAMostrarse == 3)
	{
		agregarAlbum(arregloAlbumnesTheStrokes)
		console.log(`Se ha agregado un nuevo álbum para ${artistas[indiceArtistas]}`)	
		console.log(arregloAlbumnesTheStrokes)
	}

	else
	{
		console.log('Opción inválida')
		ejecutarMenuArtistas()
	} 
}

//Menus
const ejecutarMenuPrincipal = () => {

	const menuSeleccion = [ 
		{ hotkey: '1', title: 'Registrar usuarios' }, 
		{ hotkey: '2', title: 'Logear usuarios' }
	]

	const cabeceraMenuPrincipal = [ { header: 'Menu Principal', border: true } ]

	return menu(menuSeleccion, cabeceraMenuPrincipal).then(item => {
		
		switch(item.hotkey)
		{
			case '1':
				registrarUsuarios(arregloUsuarios)
				break;

			case '2':
				logearUsuarios(arregloUsuarios)
				break;

			default:
				console.log('Opción inválida')
		}
	})
}

const ejecutarMenuArtistas = () => {

	const menuSeleccionArtistas = [
		{ hotkey: '1', title: 'Listas de artistas disponibles' },
		{ hotkey: '2', title: 'Agregar álbum' },
		{ hotkey: '3', title: 'Agregar canción' }
	]

	const cabeceraMenuArtistas = [ { header: 'Menu de artistas', border: true } ]

	return menu(menuSeleccionArtistas, cabeceraMenuArtistas).then(item => {

		switch(item.hotkey)
		{
			case '1':
				listarArtistasDisponibles(arregloArtistas)
				break;

			case '2':
				listarAlbumnesAgregados(arregloArtistas)
				break;

			case '3':
				break;

			default:
				console.log('Opción inválida')
		}
	})
}

ejecutarMenuPrincipal();
