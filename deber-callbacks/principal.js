const metodos = require('./metodos.js')

const arregloUsuarios = [
	{ nombre: 'Abigail', apellido: 'Carvajal' },
	{ nombre: 'Carlos', apellido: 'Morales' },
	{ nombre: 'Beatriz', apellido: 'Reyes' },
	{ nombre: 'Briggitte', apellido: 'Villegas' },
	{ nombre: 'Ernesto', apellido: 'Yaselga' }
]

const nuevoUsuario = { nombre: 'Andrés', apellido: 'Torres'}

metodos.crearNuevoUsuario(arregloUsuarios, nuevoUsuario, (respuesta)  => {
	console.log(respuesta)
})