module.exports = (arregloUsuarios, usuarioABuscar, callbackBusqueda) => {

	const usuariosEncontrados = arregloUsuarios.filter((usuario) => {

		if(usuarioABuscar.nombre == usuario.nombre || usuarioABuscar.apellido == usuario.apellido)
		{
			return usuario
		}
	})

	if(usuariosEncontrados)
	{
		callbackBusqueda({
			mensaje: 'Se han encontrado varias coincidencias',
			listaUsuarios: usuariosEncontrados
		})
	}

	else
	{
		callbackBusqueda({
			mensaje: 'No se encontrar multiples coincidencias',
			listaUsuarios: usuariosEncontrados
		})
	}
}