module.exports = (arregloUsuarios, indiceUsuarioAEliminar, callbackEliminar) => {

	arregloUsuarios.splice(indiceUsuarioAEliminar, 1)

	callbackEliminar({
		mensaje: 'Se ha eliminado el usuario exitosamente',
		arregloUsuarios: arregloUsuarios
	})
    
}