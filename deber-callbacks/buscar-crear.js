const buscarUsuario = require('./buscar-primera-ocurrencia.js')
const crearUsuario = require('./crear.js')

module.exports = (arregloUsuarios, nuevoUsuario, callBack) => {

    buscarUsuario(arregloUsuarios, nuevoUsuario, (resultadoBusqueda) => {

        if(resultadoBusqueda.mensaje == 'Se ha encontrado al usuario')
        {

        	callBack({
                mensaje: 'Se ha encontrado el usuario',
                usuario: resultadoBusqueda.usuarioBuscado
            })
        }

        else
        {
        	crearUsuario(arregloUsuarios, nuevoUsuario, (resultadoUsuarioCreado) => {

                callBack({
                    mensaje: 'El usuario no existía, se ha agregado al nuevo usuario',
                    usuario: resultadoUsuarioCreado.arregloUsuarios
                })
            })
        }
    })
}
