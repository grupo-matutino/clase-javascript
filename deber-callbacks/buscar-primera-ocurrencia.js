module.exports = (arregloUsuarios, usuarioABuscar, callbackUsuario) => {

    const usuarioEncontrado = arregloUsuarios.find((usuario) => {

    	if((usuarioABuscar.nombre === usuario.nombre) && (usuarioABuscar.apellido === usuario.apellido))
    	{
    		return usuario
    	}
    })

    const indiceUsuario = arregloUsuarios.findIndex((usuario) => {

    	if((usuarioABuscar.nombre === usuario.nombre) && (usuarioABuscar.apellido === usuario.apellido))
    	{
    		return usuario
    	}
    })
    
    if(usuarioEncontrado)
	{

        callbackUsuario({
            mensaje: 'Se ha encontrado al usuario',
            usuarioBuscado: usuarioABuscar,
            posicion: indiceUsuario
        })
	}

    else
    {
        callbackUsuario({
            mensaje: 'El usuario que busca no existe',
            usuarioBuscado: usuarioABuscar
        })
	}
}