module.exports = (arregloUsuarios, usuarioABuscar, callbackUsuario) => {

    const usuarioEncontrado = arregloUsuarios.find((usuario) => {

        return usuarioABuscar === usuario
    })
    
    if(usuarioEncontrado)
	{
        callbackUsuario({
            mensaje: 'Se ha encontrado al usuario',
            usuarioBuscado: usuarioABuscar
        })
	}

    else
    {
        callbackUsuario({
            mensaje: 'El usuario que busca no existe',
            usuarioBuscado: usuarioABuscar
        })
	}
}