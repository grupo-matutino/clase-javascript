const buscarUsuario = require('./buscar-primera-ocurrencia.js')
const eliminarUsuario = require('./eliminar.js')

module.exports = (arregloUsuarios, usuarioAEliminar, callBack) => {

	buscarUsuario(arregloUsuarios, usuarioAEliminar, (resultadoBusqueda) => {

		if(resultadoBusqueda.mensaje == 'Se ha encontrado al usuario')
		{

			eliminarUsuario(arregloUsuarios, resultadoBusqueda.Posicion, (resultadoEliminacion) => {
				
				callBack({

					mensaje: 'El usuario ha sido eliminado',
				})

			})
		}

		else
		{
			callBack({

				mensaje: 'El usuario que desea eliminar no existe',

			})
		}
	})
}