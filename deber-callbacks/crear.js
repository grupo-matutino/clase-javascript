module.exports = (arregloUsuarios, usuarioACrear, callbackCrear) => {

    arregloUsuarios.push(usuarioACrear)

    callbackCrear({
        mensaje: 'Se ha ingresado el usuario exitosamente',
        arregloUsuarios: arregloUsuarios
    })
}