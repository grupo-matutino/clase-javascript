const { from } = require('rxjs')

module.exports.buscarUnaOcurrenciaConCallback = (arregloUsuarios, usuarioABuscar, callbackUsuario) => {

    const usuarioEncontrado = arregloUsuarios.find((usuario) => {

    	if((usuarioABuscar.nombre === usuario.nombre))
    	{
    		return usuario
    	}
    })

    const indiceUsuario = arregloUsuarios.findIndex((usuario) => {

    	if((usuarioABuscar.nombre === usuario.nombre))
    	{
    		return usuario
    	}
    })
    
    if(usuarioEncontrado)
	{

        callbackUsuario({
            mensaje: 'Se ha encontrado al usuario',
            usuarioBuscado: usuarioABuscar,
            posicion: indiceUsuario,
            fueEncontrado: true
        })
	}

    else
    {
        callbackUsuario({
            mensaje: 'El usuario que busca no existe',
            usuarioBuscado: usuarioABuscar,
            fueEncontrado: false
        })
	}
}

module.exports.buscarUnaOcurrenciaConPromesas = (arregloUsuarios, usuarioABuscar) => {
	
    variableBuscarUnaOcurrenciaConPromesas = (resolve, reject) => {

        let resultadoBusquedaConPromesas = {
            usuarioBuscado: null,
            posicion: null,
            fueEncontrado: false
        }

        const usuarioEncontrado = arregloUsuarios.find((usuario, indiceUsuario) => {

            if((usuarioABuscar.nombre === usuario.nombre))
            {
                resultadoBusquedaConPromesas = {
                    mensaje: 'Se ha encontrado al usuario',
                    usuarioBuscado: usuarioABuscar,
                    posicion: indiceUsuario,
                    fueEncontrado: true
                }
            }
        })

        const usuarioNoEncontrado = {
            mensaje: 'El usuario que busca no existe',
            usuarioBuscado: usuarioABuscar,
            fueEncontrado: false
        }

        resolve(resultadoBusquedaConPromesas)
        reject(usuarioNoEncontrado)

    }

	return new Promise(variableBuscarUnaOcurrenciaConPromesas)
}

module.exports.buscarUnaOcurrenciaConObservables = (arregloUsuarios, usuarioABuscar) => {

    let resultadoBusquedaConObservables = {
        usuarioBuscado: null,
        posicion: null,
        fueEncontrado: null
    }

    const usuarioEncontrado = arregloUsuarios.find((usuario, indiceUsuario) => {

        if((usuarioABuscar.nombre === usuario.nombre))
        {
            resultadoBusquedaConObservables = {
                mensaje: 'Se ha encontrado al usuario',
                usuarioBuscado: usuarioABuscar,
                posicion: indiceUsuario,
                fueEncontrado: true
            }

        }
    })

    return from(usuarioEncontrado)

}