const menu = require('console-menu')
const metodos = require('./metodos.js')
const lectorConsola = require('readline-sync')
const usuarios = require('./datos-prueba.js')

const menuSeleccion = [
	{ hotkey: '1', title: 'Buscar un usuario' },
	{ hotkey: '2', title: 'Buscar múltiples ocurrencias' },
	{ hotkey: '3', title: 'Buscar y crear' },
	{ hotkey: '4', title: 'Crear un usuario' },
	{ hotkey: '5', title: 'Eliminar un usuario' }
]

module.exports.ejecutarMenuCallbacks = () => {

	const cabeceraMenuCallbacks = [ { header: 'MENÚ DE FUNCIONES CON CALLBACK', border: true } ]

	return menu(menuSeleccion, cabeceraMenuCallbacks).then(item => {

		switch(item.hotkey)
		{
			case '1':
				ejecutarBusquedaUsuarioUnitario(usuarios.arregloUsuarios, usuarios.usuarioABuscar)
				break;

			case '2':
				ejecutarBusquedaMultiplesOcurrencias(usuarios.arregloUsuarios)
				break;

			case '3':
				ejecutarBuscarYCrear(usuarios.arregloUsuarios, usuarios.usuarioACrear)
				break;

			case '4':
				ejecutarCrearUsuario(usuarios.arregloUsuarios, usuarios.usuarioACrear)
				break;

			case '5':
				ejecutarEliminarUsuario(usuarios.arregloUsuarios, usuarios.usuarioAEliminar)
				break;

			default:
				console.log('Opción inválida')
		}
	})
}

const ejecutarBusquedaUsuarioUnitario = (listaUsuarios, usuarioABuscar) => {
	
	metodos.buscarUnUsuario.buscarUnaOcurrenciaConCallback(listaUsuarios, usuarioABuscar, (respuesta) => {

		console.log(respuesta)
	})

	ejecutarMenuCallbacks()
}

const ejecutarBusquedaMultiplesOcurrencias = (listaUsuarios) => {

	criterioDeSeleccion = lectorConsola.question('Ingrese parte del nombre a buscar: ')
	
	metodos.buscarTodosLosUsuarios.buscarMultiplesOcurrenciasConCallback(listaUsuarios, criterioDeSeleccion, (respuesta) => {

		console.log(respuesta)
	})

	ejecutarMenuCallbacks()
}

const ejecutarBuscarYCrear = (listaUsuarios, usuarioACrear) => {
	
	metodos.buscarYCrear.buscarYCrearConCallback(arregloUsuarios, usuarioACrear, (respuesta) => {

		console.log(respuesta)
	})

	ejecutarMenuCallbacks()
}

const ejecutarCrearUsuario = (listaUsuarios, usuarioACrear) => {
	
	metodos.crearNuevoUsuario.crearConCallback(arregloUsuarios, usuarioACrear, (respuesta) => {

		console.log(respuesta)
	})

	ejecutarMenuCallbacks()
}

const ejecutarEliminarUsuario = (listaUsuarios, usuarioAEliminar) => {
	
	metodos.eliminarUsuario.eliminarUsuarioConCallback(arregloUsuarios, usuarioAEliminar, (respuesta) => {

		console.log(respuesta)
		console.log(arregloUsuarios)
	})

	ejecutarMenuCallbacks()
}
