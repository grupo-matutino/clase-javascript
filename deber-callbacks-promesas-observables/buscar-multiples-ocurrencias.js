module.exports.buscarMultiplesOcurrenciasConCallback = (arregloUsuarios, criterioDeSeleccion, callbackBusqueda) => {

	const arregloFiltradoUsuarios = arregloUsuarios.filter(usuarios => {

		return usuarios.nombre.search(criterioDeSeleccion) > -1
	})

	if(arregloFiltradoUsuarios)
	{
		callbackBusqueda({
			mensaje: 'Se han encontrado múltiples ocurrencias',
			usuariosEncontrados: arregloFiltradoUsuarios
		})
	}

	else
	{
		callbackBusqueda({
			mensaje: 'No se han encontrado coincidencias',
			usuariosEncontrados: arregloFiltradoUsuarios
		})
	}

}

module.exports.buscarMultiplesOcurrenciasConPromesas = (arregloUsuarios, criterioDeSeleccion) => {

	const arregloFiltradoUsuarios = arregloUsuarios.filter(usuarios => {

		return usuarios.nombre.search(criterioDeSeleccion) > -1
	})

	return new Promise((resolve, reject) => {

		if(arregloFiltradoUsuarios)
		{
			resolve({
				mensaje: 'Se han encontrado múltiples ocurrencias',
				usuariosEncontrados: arregloFiltradoUsuarios
			})
		}

		else
		{
			reject({
				mensaje: 'No se han encontrado coincidencias',
				usuariosEncontrados: arregloFiltradoUsuarios
			})
		}
	})
}