const buscarUsuario = require('./buscar-primera-ocurrencia.js')
const crearUsuario = require('./crear.js')

module.exports.buscarYCrearConCallback = (arregloUsuarios, nuevoUsuario, callback) => {

    buscarUsuario.buscarUnaOcurrenciaConCallback(arregloUsuarios, nuevoUsuario, (resultadoBusqueda) => {

        if(resultadoBusqueda.fueEncontrado)
        {
        	callback({
                mensaje: 'Se ha encontrado el usuario',
                usuario: resultadoBusqueda.usuarioBuscado
            })
        }

        else
        {
        	crearUsuario.crearConCallback(arregloUsuarios, nuevoUsuario, (resultadoUsuarioCreado) => {

                callback({
                    mensaje: 'El usuario no existía, se ha agregado al nuevo usuario',
                    usuario: resultadoUsuarioCreado.arregloUsuarios
                })
            })
        }
    })
}

module.exports.buscarYCrearConPromesas = (arregloUsuarios, nuevoUsuario) => {

    let variableBuscarYCrearConPromesas = (resolve, reject) => {

        let existeElUsuario = false

        buscarUsuario.buscarUnaOcurrenciaConPromesas(arregloUsuarios, nuevoUsuario).then((resultadoBuscarConPromesas) => {

            existeElUsuario = resultadoBuscarConPromesas.fueEncontrado

            if(existeElUsuario)
            {
                reject({
                    mensaje: 'El usuario que desea ingresar ya existe',
                    arregloUsuarios: arregloUsuarios
                })
            }

            else 
            {
                crearUsuario.crearConPromesas(arregloUsuarios, nuevoUsuario).then((resultadoCrearConPromesas) => {
                    resolve({
                        mensaje: 'El usuario que ha buscado no existía y ha sido creado',
                        arregloUsuarios: arregloUsuarios
                    })
                })
            }
        })
        .catch((errorEnLaBusqueda) => {
            console.log(errorEnLaBusqueda)
        })
    } 

    return new Promise(variableBuscarYCrearConPromesas)
}
