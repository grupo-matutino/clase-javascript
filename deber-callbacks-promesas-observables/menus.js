const menuCallbacks = require('./menu-callbacks.js')
const menuPromesas = require('./menu-promesas.js')

module.exports = [
	menuCallbacks,
	menuPromesas
]