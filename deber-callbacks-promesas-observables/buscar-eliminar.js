const buscarUsuarioAEliminar = require('./buscar-primera-ocurrencia.js')
const eliminarUsuario = require('./borrar.js')

module.exports.eliminarUsuarioConCallback = (arregloUsuarios, usuarioAEliminar, callback) => {

	buscarUsuarioAEliminar.buscarUnaOcurrenciaConCallback(arregloUsuarios, usuarioAEliminar, (resultadoBusqueda) => {

		if(resultadoBusqueda.fueEncontrado)
		{
			eliminarUsuario.eliminarConCallback(arregloUsuarios, resultadoBusqueda.posicion, (resultadoEliminacion) => {
				
				callback({

					mensaje: 'El usuario ha sido eliminado'
				})
			})
		}

		else
		{
			callback({

				mensaje: 'El usuario que desea eliminar no existe'
			})
		}
	})
}

module.exports.eliminarUsuarioConPromesas = (arregloUsuarios, usuarioAEliminar) => {

	let variableEliminarUsuarioConPromesas = (resolve, reject) => {

		let existeElUsuario = false

		buscarUsuarioAEliminar.buscarUnaOcurrenciaConPromesas(arregloUsuarios, usuarioAEliminar).then((resultadoBuscarConPromesas) => {

			existeElUsuario = resultadoBuscarConPromesas.fueEncontrado

            if(existeElUsuario)
            {
            	eliminarUsuario.eliminarConPromesas(arregloUsuarios, usuarioAEliminar).then((resultadoEliminarConPromesas) => {
            		resolve({
                    	mensaje: 'El usuario ha sido eliminado exitosamente',
                    	arregloUsuarios: arregloUsuarios
                	})
            	})
            }

            else 
            {
            	reject({
            		mensaje: 'El usuario que desea eliminar no existe',
            		arregloUsuarios: arregloUsuarios
            	})
            }
        })
        .catch((errorEnLaBusqueda) => {
            console.log(errorEnLaBusqueda)
        })
	}

	return new Promise(variableEliminarUsuarioConPromesas)
}