module.exports.eliminarConCallback = (arregloUsuarios, indiceUsuarioAEliminar, callbackEliminar) => {

	arregloUsuarios.splice(indiceUsuarioAEliminar, 1)

	callbackEliminar({
		mensaje: 'Se ha eliminado el usuario exitosamente'
	})
}

module.exports.eliminarConPromesas = (arregloUsuarios, indiceUsuarioAEliminar) => {

	const usuarioEliminado = arregloUsuarios.splice(indiceUsuarioAEliminar, 1)

	return new Promise((resolve, reject) => {

		if(usuarioEliminado)
		{
			resolve({
				mensaje: 'Se ha eliminado el usuario exitosamente'
			})
		}

		else
		{
			reject({
				mensaje: 'No se ha podido eliminar al usuario'
			})
		}
	})
}