module.exports.crearConCallback = (arregloUsuarios, usuarioACrear, callbackCrear) => {

    arregloUsuarios.push(usuarioACrear)

    callbackCrear({
        mensaje: 'Se ha ingresado el usuario exitosamente',
        arregloUsuarios: arregloUsuarios
    })
}

module.exports.crearConPromesas = (arregloUsuarios, usuarioACrear) => {

	const usuarioCreado = arregloUsuarios.push(usuarioACrear)

	return new Promise((resolve, reject) => {

		if(usuarioCreado)
		{
			resolve({
				mensaje: 'Se ha ingresado el usuario exitosamente',
				arregloUsuarios: arregloUsuarios
			})
		}

		else
		{
			reject({
				mensaje: 'El usuario no pudo ser ingresado',
				arregloUsuarios: arregloUsuarios
			})
		}
	})

}