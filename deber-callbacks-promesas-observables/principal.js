const menu = require('console-menu')
const lectorConsola = require('readline-sync')
const listaMenus = require('./menus.js')

const menuSeleccion = [
	{ hotkey: '1', title: 'Utilizando callbacks' },
	{ hotkey: '2', title: 'Utilizando promesas' },
	{ hotkey: '3', title: 'Utilizando observables' },
]

const ejecutarMenuPrincipal = () => {

	const cabeceraMenuPrincipal = [ { header: 'MENÚ DE FUNCIONES', border: true } ]

	return menu(menuSeleccion, cabeceraMenuPrincipal).then(item => {

		switch(item.hotkey)
		{
			case '1':
				listaMenus.menuCallbacks()
				break;

			case '2':
				listaMenus.menuPromesas()
				break;

			case '3':
				break;

			default:
				console.log('Opción inválida')
		}
	})
}

ejecutarMenuPrincipal();












