module.exports.arregloUsuarios = [
	{ nombre: 'Abigail' },
	{ nombre: 'Carlos' },
	{ nombre: 'Beatriz' },
	{ nombre: 'Briggitte' },
	{ nombre: 'Ernesto' }
]

module.exports.usuarioABuscar = { nombre: 'Beatriz' }

module.exports.usuarioACrear = { nombre: 'Andrés' }

module.exports.usuarioABuscarYCrear = { nombre: 'Stephanie' }
 
module.exports.usuarioAEliminar = { nombre: 'Briggitte' }