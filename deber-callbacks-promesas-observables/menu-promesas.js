const menu = require('console-menu')
const metodos = require('./metodos.js')
const lectorConsola = require('readline-sync')
const usuarios = require('./datos-prueba.js')

const menuSeleccion = [
	{ hotkey: '1', title: 'Buscar un usuario' },
	{ hotkey: '2', title: 'Buscar múltiples ocurrencias' },
	{ hotkey: '3', title: 'Buscar y crear' },
	{ hotkey: '4', title: 'Crear un usuario' },
	{ hotkey: '5', title: 'Eliminar un usuario' }
]

module.exports.ejecutarMenuPromesas = () => {

	const cabeceraMenuPromesas = [ { header: 'MENÚ DE FUNCIONES CON CALLBACK', border: true } ]

	return menu(menuSeleccion, cabeceraMenuPromesas).then(item => {

		switch(item.hotkey)
		{
			case '1':
				ejecutarBusquedaUsuarioUnitario(usuarios.arregloUsuarios, usuarios.usuarioABuscar)
				break;

			case '2':
				ejecutarBusquedaMultiplesOcurrencias(usuarios.arregloUsuarios)
				break;

			case '3':
				ejecutarBuscarYCrear(usuarios.arregloUsuarios, usuarios.usuarioACrear)
				break;

			case '4':
				ejecutarCrearUsuario(usuarios.arregloUsuarios, usuarios.usuarioACrear)
				break;

			case '5':
				ejecutarEliminarUsuario(usuarios.arregloUsuarios, usuarios.usuarioAEliminar)
				break;

			default:
				console.log('Opción inválida')
		}
	})
}

const ejecutarBusquedaUsuarioUnitario = (listaUsuarios, usuarioABuscar) => {

	metodos.buscarUnUsuario.buscarUnaOcurrenciaConPromesas(listaUsuarios, usuarioABuscar).then((respuesta) => {

		console.log(respuesta)
	})
	.catch((errorPromesa) => {

		console.log(errorPromesa)
	})

	ejecutarMenuPromesas()
}

const ejecutarBusquedaMultiplesOcurrencias = (listaUsuarios) => {

	criterioDeSeleccion = lectorConsola.question('Ingrese parte del nombre a buscar: ')
	
	metodos.buscarTodosLosUsuarios.buscarMultiplesOcurrenciasConPromesas(listaUsuarios, criterioDeSeleccion).then((respuesta) => {

		console.log(respuesta)
	})
	.catch((errorPromesa) => {

		console.log(errorPromesa)
	})

	ejecutarMenuPromesas()
}

const ejecutarBuscarYCrear = (listaUsuarios, usuarioACrear) => {
	
	metodos.buscarYCrear.buscarYCrearConPromesas(listaUsuarios, usuarioACrear).then((respuesta) => {

		console.log(respuesta)
	})
	.catch((errorPromesa) => {

		console.log(errorPromesa)
	})

	ejecutarMenuPromesas()
}

const ejecutarCrearUsuario = (listaUsuarios, usuarioACrear) => {
	
	metodos.crearNuevoUsuario.crearConPromesas(listaUsuarios, usuarioACrear).then((respuesta) => {

		console.log(respuesta)
	})
	.catch((errorPromesa) => {

		console.log(errorPromesa)
	})

	ejecutarMenuPromesas()
}

const ejecutarEliminarUsuario = (listaUsuarios, usuarioAEliminar) => {
	
	metodos.eliminarUsuario.eliminarUsuarioConPromesas(arregloUsuarios, usuarioABuscar).then((respuesta) => {

		console.log(respuesta)
	})
	.catch((errorPromesa) => {

		console.log(errorPromesa)
	})

	ejecutarMenuPromesas()
}
