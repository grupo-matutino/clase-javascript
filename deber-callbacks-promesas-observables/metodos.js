const buscarYCrear = require('./buscar-crear.js')
const buscarTodosLosUsuarios = require('./buscar-multiples-ocurrencias')
const buscarUnUsuario = require('./buscar-primera-ocurrencia.js')
const crearNuevoUsuario = require('./crear.js')
const eliminarUsuario = require('./buscar-eliminar.js')

module.exports = {
	buscarYCrear,
    buscarTodosLosUsuarios,
    buscarUnUsuario,
    crearNuevoUsuario,
    eliminarUsuario
}