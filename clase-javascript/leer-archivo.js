const fs = require('fs')
const crearArchivo = require('./crear-archivo.js')

module.exports = (path) => {

    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', (errorAlLeer) => {
            if(error)
            {
                reject({
                    Mensaje: 'A ocurrido un error',
                    ERROR: errorAlLeer
                })
            }

            else 
            {
                resolve({
                    Mensaje: 'Archivo leido exitosamente',
                    ERROR: errorAlLeer
                })
            }
        })
    })
}