const fs = require('fs')

module.exports = (archivo, mensaje, cb) => {

    fs.writeFile(archivo, mensaje ,(error) => {

        if(error)
        {
            cb({
                    ERROR: error,
                    archivoCreado: false,
                    Info: mensaje
                })
        }

        else
        {
            cb({
                ERROR: error,
                archivoCreado: true,
                Info: mensaje
            })

        }


    })

}