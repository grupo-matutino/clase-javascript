const random = require('random-js')()
const moment = require('moment')

//Dado un arreglo con el atributo nombre, agregar una fecha de nacimiento aleatoria desde 1930 a 2018, escribir una funcion que calcule la edad y filtrar a: menores de edad, jovenes y adultos mayores

const arregloPersonas = [
    { Nombre: 'Abigail' }, { Nombre: 'Alexander' }, { Nombre: 'Andres' }, { Nombre: 'Beatriz' },
    { Nombre: 'Briggitte' }, { Nombre: 'Carlos' }, { Nombre: 'Ernesto' }, { Nombre: 'Karina' },
    { Nombre: 'Mishelle' }, { Nombre: 'Solange' }
]

const arregloMenoresEdad = []
const arregloJovenes = []
const arregloMayoresEdad = []

var nuevoArregloPersonas = arregloPersonas.map((valor) => {

    valor.fechaNacimiento = `${random.integer(1930, 2018)}-${random.integer(1, 12)}-${random.integer(1, 31)}`
    valor.edadPersona = calcularEdad(valor.fechaNacimiento)

    return valor
})

arregloMenoresEdad = nuevoArregloPersonas.reduce((acumulador, valor) => {

    if(valor.edadPersona > 0 && valor.edadPersona < 18)
    {
        acumulador.push(valor)       
    }

    return acumulador

}, [])

arregloJovenes = nuevoArregloPersonas.reduce((acumulador, valor) => {

    if(valor.edadPersona > 18 && valor.edadPersona < 60)
    {
        acumulador.push(valor)       
    }

    return acumulador
}, [])

arregloMayoresEdad = nuevoArregloPersonas.reduce((acumulador, valor) => {

    if(valor.edadPersona > 60)
    {
        acumulador.push(valor)       
    }

    return acumulador
}, [])

function calcularEdad(fechaPersona)
{
    var fechaActual = moment().year()
    var fechaNacimiento = moment(fechaPersona).year()

    return fechaActual - fechaNacimiento
}

console.log('Las personas menores de edad son: ')
console.log(arregloMenoresEdad)
console.log('Las personas jóvenes-adultos son:')
console.log(arregloJovenes)
console.log('Los adultos mayores son:')
console.log(arregloMayoresEdad)