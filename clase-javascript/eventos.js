const EventEmitter = require('events');

class EntroALaCasa extends EventEmitter{

}

const entroALaCasa = new EntroALaCasa();

entroALaCasa.on('por la puerta delantera',(nombre)=>{
    console.log(`Saludar a ${nombre} quien entró por la puerta delantera`)

})

entroALaCasa.emit('por la puerta delantera', 'Adler')

entroALaCasa.on('por la puerta trasera',(nombre)=>{
    console.log(`Saludar a ${nombre} quien entró por la puerta delantera`)

})

entroALaCasa.emit('por la puerta trasera', 'Beatriz')

entroALaCasa.on('por la ventana',()=>{
    console.log('LLamar a la policía, un extraño entró por la ventana')
})

entroALaCasa.emit('por la ventana')

/* Ejemplo de evento */

class TocarTimbreBus extends EventEmitter{

}

const tocarTimbreBus = new TocarTimbreBus();

tocarTimbreBus.on('de la puerta trasera', (nombreParada)=>{
    console.log(`Esta es mi parada, ${nombreParada}, ¡pare el bus chofer!`)
})

tocarTimbreBus.emit('de la puerta trasera','San Roque')

