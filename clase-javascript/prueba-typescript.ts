const persona:object = {
    nombre: 'Adler',
    apellido: 'Luft'
}

const edad:number = 22
const nombre:string = 'Beatriz'
let numero:number

const estado:boolean = true;
//En ambos casos, se pueden definir distintos tipos de variables
const arreglo:Array<number | string | boolean> = [] //Permite definir distintos tipos de variables
const arreglo2:(number | string)[] = [] //Más utilizado
let variableCualquiera:any //Se utiliza cuando se desconoce el tipo de la variable

function sumar(a?:number,b?:number):number //El signo ? sirve para especificar variables opcionales
{
    return a+b
}

sumar(2, 4)
sumar(2)
sumar()

class usuarioBaseConGetYSet
{
    //Las variables privadas siempre se escriben con un guión bajo al comienzo del nombre de la variable
    private _nombre:string;
    private _apellido:string;
    public edad:number;

    constructor(nombre:string, apellido:string)
    {
        this._nombre = nombre;
        this._apellido = apellido;
        this.edad = edad;
    }
    
    get nombre()
    {
        return this._nombre
    }

    set nombre(nombre:string)
    {
        this._nombre = nombre
    }

    get apellido()
    {
        return this._apellido
    }

    set apellido(apellido:string)
    {
        this._apellido = apellido
    }
}

class usuarioBase
{
    public edad

    constructor(protected nombre:string, protected apellido:string)
    {
        this.nombre = nombre
        this.apellido = apellido
        this.edad = edad;
    }
}

class estudiantePoli extends usuarioBase
{
    constructor(public nombre:string, public apellido:string, public carrera:string, public numeroUnico:number)
    {
        super(nombre, apellido)
        this.carrera = carrera
        this.numeroUnico = numeroUnico
    }
}

const nuevoEstudiante = new estudiantePoli('Andrés', 'Torres', 'Sistemas', 20142056)
console.log('Estudiante: ', nuevoEstudiante)

class estudiantePoliConGetYSet extends usuarioBaseConGetYSet
{
    constructor(public nombre:string, public apellido:string, public carrera:string, public numeroUnico:number)
    {
        super(nombre, apellido)
        this.carrera = carrera
        this.numeroUnico = numeroUnico
    }
}

const otroEstudiante = new estudiantePoliConGetYSet('Mateo', 'Narváez', 'Mecánica', 20142258)
console.log('Estudiante con get y set: ', otroEstudiante)

class empleadoSupermaxi extends usuarioBase
{
    constructor(public nombre:string, public apellido:string, public cargo:string)
    {
        super(nombre, apellido)
        this.cargo = cargo
    }
}

const nuevoEmpleado = new empleadoSupermaxi('Lalo', 'Landa', 'cajero')
console.log('Empleado de supermaxi: ', nuevoEmpleado)

class empleadoSupermaxiConGetYSet extends usuarioBaseConGetYSet
{
    constructor(public nombre:string, public apellido:string, public cargo:string)
    {
        super(nombre, apellido)
        this.cargo = cargo
    }
}

const otroEmpleado = new empleadoSupermaxiConGetYSet('Rita', 'Laso', 'bodeguera')
console.log('Empleado con get y set: ', otroEmpleado)

class usuarioPolicia extends usuarioBase
{
    constructor(public nombre:string, public apellido:string, public grado:string)
    {
        super(nombre, apellido)
        this.grado = grado
    }
}

const nuevoPolicia = new usuarioPolicia('Paco', 'Moncayo', 'coronel')
console.log('Miembro del cuerpo policial: ', nuevoPolicia)

class usuarioPoliciaConGetYSet extends usuarioBaseConGetYSet
{
    constructor(public nombre:string, public apellido:string, public grado:string)
    {
        super(nombre, apellido)
        this.grado = grado
    }
}

const otroPolicia = new usuarioPoliciaConGetYSet('Augustus', 'Sinclair', 'policía')
console.log('Miembro del cuerpo policial con get y set: ', otroPolicia)