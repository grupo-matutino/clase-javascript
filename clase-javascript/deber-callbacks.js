const arregloUsuarios = [
	{ Nombre: 'Abigail', Apellido: 'Carvajal' },
	{ Nombre: 'Carlos', Apellido: 'Morales' },
	{ Nombre: 'Beatriz', Apellido: 'Reyes' },
	{ Nombre: 'Briggitte', Apellido: 'Villegas' },
	{ Nombre: 'Ernesto', Apellido: 'Yaselga' }
]

function buscarUsuario(nombreUsuario, apellidoUsuario)
{
	arregloUsuarios.forEach((valor, indice) => {

		if(nombreUsuario == valor.Nombre && apellidoUsuario == valor.Apellido)
		{
			console.log('Se ha encontrado al usuario', nombreUsuario, apellidoUsuario)
		}

		else
		{
			console.log('El usuario que busca no existe')
		}

		return valor
	})
}

function crearUsuario(nombreUsuario, apellidoUsuario)
{
	arregloUsuarios.push({Nombre: nombreUsuario, Apellido: apellidoUsuario})

	console.log('Se ha agregado exitosamente el nuevo usuario, el arreglo queda:')
	console.log(arregloUsuarios)

	return arregloUsuarios
}

buscarYCrearUsuario = (nombreUsuario, apellidoUsuario, cb) => {

	arregloUsuarios.forEach((valor, indice) => {

		if(nombreUsuario == valor.Nombre && apellidoUsuario == valor.Apellido)
		{
			console.log('Se ha encontrado al usuario', nombreUsuario, apellidoUsuario)
		}

		else
		{
			crearUsuario(nombreUsuario, apellidoUsuario, (cb) => {

				console.log('El usuario buscado ha sido ingresado')
			})
		}

		return valor
	})
}

buscarUsuario('Carlos', 'Morales')
crearUsuario('Andres', 'Torres')
buscarYCrearUsuario('Adriana', 'Flores')