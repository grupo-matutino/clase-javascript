/* const saludar = require('./saludar.js')

function peticionUsuario(cb)
{
    cb('Hola mundo')
}

peticionUsuario(saludar) */

const leerArchivo = require('./leer-archivo.js')

leerArchivo('ejemplo1.txt', 'Hello there!', (resultadoMostrado) => {
    if(resultadoMostrado.crearArchivo)
    {
        console.log('Se creo el archivo')
    }

    else
    {
        console.log('No se creo el archivo')
    }
})