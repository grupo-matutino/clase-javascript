//Agregar edades a los dueños y a los perros y realizar la suma de ambos, y luego filtrar a dueños masculinos como a femeninos

var dueñoMascota = [
    { Nombre: 'Abigail', Sexo: 'Femenino', Mascota: 'Baubau'},
    { Nombre: 'Neil',  Sexo: 'Masculino', Mascota: 'Cabal'},
    { Nombre: 'Ernest',  Sexo: 'Masculino', Mascota: 'Snowball'},
]

function sumarEdades(edadDueño, edadMascota)
{
    return edadDueño + edadMascota
}

const nuevoArregloDueños = dueñoMascota.map((valor) => {
    
    valor.edadDueño = Math.floor(Math.random()*100)
    valor.edadMascota = Math.floor(Math.random()*10)
    valor.sumaTotalEdades = sumarEdades(valor.edadDueño, valor.edadMascota)

    return valor
}).reduce((acumuladorMasculino, valor) => {
    
    var esMasculino = valor.Sexo === 'Masculino'

    if(esMasculino)
    {
        acumuladorMasculino.push(valor)
    }

    return acumuladorMasculino
}, []).filter((valor) => {

    if(valor.edadMascota % 2 != 0)
    {
        return valor
    }
})

console.log(nuevoArregloDueños)
