/* interface usuarioInterface {
    nombre:string;
    apellido:string;
}

const usuarioInterfaz:usuarioInterface = {
    nombre: 'Neil',
    apellido: 'Gaiman'
}

console.log('Usuario interfaz (Antes): ', usuarioInterfaz)

usuarioInterfaz.nombre = 'Julio'
usuarioInterfaz.apellido = 'Verne'

console.log('Usuario interfaz (Después): ',usuarioInterfaz)

interface correoInterface {
    correo:string;
    enviarCorreo():boolean;
}

interface global extends usuarioInterface, correoInterface{
    estaCompleto:boolean;
}

class usuarioCualquiera implements global
{
    estaCompleto: boolean;    
    nombre: string;
    apellido: string;
    correo: string;

    enviarCorreo(): boolean {
        throw new Error("Method not implemented.");
    }
}

 */