var noticia = 'Su objetivo era que en 2030 el Estado "ejerza los derechos de los segmentos correspondientes en la órbita sincrónica geoestacionaria". Pero la misión terminó. El Instituto Espacial Ecuatoriano (IEE) será una de las cinco instituciones que se suprimirán, según dispuso el martes el presidente de la República Lenín Moreno como parte de un paquete de medidas económicas. Sus funciones las asumirá el Instituto Geográfico Militar. La misión de esta institución, que el pasado 19 de julio cumplió seis años de creación y que estuvo dirigida por Eduardo René Castillo Camacho desde el 16 de junio de 2017, era "mantener e impulsar la investigación científica y desarrollo tecnológico espacial y el incremento de la cultura aeroespacial, que contribuyan a la Defensa y Desarrollo Nacional", según se describe en su sitio web. Para su funcionamiento asumió al Centro de Levantamientos Integrados de Recursos Naturales por Sensores Remotos (Clirsen), incluidos sus bienes muebles e inmuebles en Quito. En 2017 manejó un presupuesto de 2856500,58 destinado a Gestión institucional, Desvinculación por optimización del talento humano, Investigación y estudios para la defensa, y Generación de geoinformación para la gestión del territorio a nivel nacional, según su último informe de labores. Uno de los proyectos que manejó en 2017, financiado por la Senplades (Secretaría Nacional de Planificación y Desarrollo), fue para "generar geoinformación multipropósito del territorio nacional a escala 1: 25 000, a través del establecimiento de una infraestructura organizacional, con la participación de entidades públicas, privadas, universidades y/o escuelas politécnicas". Entre sus actividades, hechas en 2017, estuvieron el primer taller de evaluación nacional de la degradación y manejo sostenible de la tierra; la entrega de geoinformación espacial del Patrimonio de Áreas Naturales del Estado (PANE) al Ministerio de Ambiente; la reunión con representantes de la Agencia Espacial Sudafricana; la organización de la quinta edición del evento científico técnico con la temática “la exploración de nuevos planetas en el espacio”; entre otros. Según un informe de 2014, el IEE tenía en diciembre de ese año una plantilla de 157 trabajadores (21 militares, 116 servidores públicos entre los que tenían nombramiento y los que tenían contrato, y 20 bajo el régimen del Código de Trabajo).'

function cambioLetrasMayusculas(noticia)
{

}

//Al encontrar un punto, guardarlo en una nueva variable
function encontrarPunto(noticia)
{
    return noticia.split('.')
}

var noticiaDividida = encontrarPunto(noticia)
//console.log(noticiaDividida)

//Contar punto y comas 
function contarPuntoYComa(noticia)
{
    var contadorPuntoYComa = 0
    var puntoYComa = noticia.indexOf(";")

    while(puntoYComa >= 0)
    {
        puntoYComa = noticia.indexOf(";", puntoYComa+1)
        contadorPuntoYComa++
    }

    return contadorPuntoYComa
}

var guardarContador = contarPuntoYComa(noticia)
console.log(`Existe un total de ${guardarContador} punto y comas`)

//Reemplazo de espacios en blanco
function reemplazarEspaciosEnBlanco(noticia)
{
    return noticia.replace(/\s/g, '*-*')
}

var noticiaReemplazada = reemplazarEspaciosEnBlanco(noticia)
//console.log(noticiaReemplazada)
