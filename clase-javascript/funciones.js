/**
 * @author Andres Torres Albuja
 * 
 * @function Sumar
 * @param {number} a Valor entero 
 * @param {number} b Valor entero
 * @description La funcion Sumar recibe dos parametros enteros a y b. En caso de que uno de los valores sea de tipo boolean, null o decimal, se realiza la operacion. Caso contrario, se concatenaran los valores
 * @returns {number}  Valor entero
*/
 
/**
 * @function Restar
 * @param {number} a Valor entero
 * @param {number} b Valor entero
 * @description La funcion Restar recibe dos parametros enteros a y b, sin embargo, dichos parametros tambien pueden ser de tipo boolean, null o decimal.
 * @return {number} Valor entero
 * 
*/

/** 
 * @function Multiplicar
 * @param {number} a Valor entero
 * @param {number} b Valor entero
 * @description La funcion Multiplicar recibe dos parametros enteros a y b, sin embargo, dichos parametros tambien pueden ser de tipo boolean, null o decimal.
 * @return {number} Valor entero
*/

/**
 * @function Dividir
 * @param {number} a Valor entero
 * @param {number} b Valor entero
 * @description La funcion Dividir recibe dos parametros enteros a y b, sin embargo, dichos parametros tambien pueden ser de tipo boolean, null o decimal.
 * @return {number} Valor entero
 */

 /**
 * @function Modulo
 * @param {number} a Valor entero
 * @param {number} b Valor entero
 * @description La funcion Modulo recibe dos parametros enteros a y b, sin embargo, dichos parametros tambien pueden ser de tipo boolean, null o decimal.
 * @return {number} Valor entero
 */

var funciones = {

    Sumar: (a,b)=> {
        return a+b
    },

    Restar: function(a,b){
        return a-b
    },

    Multiplicar: (a,b)=>{
        return a*b
    },

    Dividir: function(a,b){
        return a/b
    },

    Modulo: (a,b)=>{
        return a%b
    }
}

arcanine = {
    forms: [
        {
            url: "https://pokeapi.co/api/v2/pokemon-form/59/",
            name: "arcanine"
        }
    ],
    abilities: [
        {
            slot: 3,
            is_hidden: true,
            ability: {
                url: "https://pokeapi.co/api/v2/ability/154/",
                name: "justified"
            }
        },
        {
            slot: 2,
            is_hidden: false,
            ability: {
                url: "https://pokeapi.co/api/v2/ability/18/",
                name: "flash-fire"
            }
        },
        {
            slot: 1,
            is_hidden: false,
            ability: {
                url: "https://pokeapi.co/api/v2/ability/22/",
                name: "intimidate"
            }
        }
    ],
    stats: [
        {
            stat: {
                url: "https://pokeapi.co/api/v2/stat/6/",
                name: "speed"
            },
            effort: 0,
            base_stat: 95
        },
        {
            stat: {
                url: "https://pokeapi.co/api/v2/stat/5/",
                name: "special-defense"
            },
            effort: 0,
            base_stat: 80
        },
        {
            stat: {
                url: "https://pokeapi.co/api/v2/stat/4/",
                name: "special-attack"
            },
            effort: 0,
            base_stat: 100
        },
        {
            stat: {
                url: "https://pokeapi.co/api/v2/stat/3/",
                name: "defense"
            },
            effort: 0,
            base_stat: 80
        },
        {
            stat: {
                url: "https://pokeapi.co/api/v2/stat/2/",
                name: "attack"
            },
            effort: 2,
            base_stat: 110
        },
        {
            stat: {
                url: "https://pokeapi.co/api/v2/stat/1/",
                name: "hp"
            },
            effort: 0,
            base_stat: 90
        }
    ],
    name: "arcanine",
    weight: 1550,
    moves: [
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/29/",
                name: "headbutt"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/34/",
                name: "body-slam"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/36/",
                name: "take-down"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/38/",
                name: "double-edge"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/43/",
                name: "leer"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/44/",
                name: "bite"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/46/",
                name: "roar"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/52/",
                name: "ember"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/53/",
                name: "flamethrower"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/63/",
                name: "hyper-beam"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/70/",
                name: "strength"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/76/",
                name: "solar-beam"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/82/",
                name: "dragon-rage"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/91/",
                name: "dig"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/92/",
                name: "toxic"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/99/",
                name: "rage"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/100/",
                name: "teleport"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/102/",
                name: "mimic"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/104/",
                name: "double-team"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/115/",
                name: "reflect"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/117/",
                name: "bide"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/126/",
                name: "fire-blast"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/129/",
                name: "swift"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/130/",
                name: "skull-bash"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/156/",
                name: "rest"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/2/",
                        name: "yellow"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/1/",
                        name: "red-blue"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/164/",
                name: "substitute"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/168/",
                name: "thief"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/172/",
                name: "flame-wheel"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/173/",
                name: "snore"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/174/",
                name: "curse"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/182/",
                name: "protect"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/189/",
                name: "mud-slap"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/200/",
                name: "outrage"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/203/",
                name: "endure"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/207/",
                name: "swagger"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/213/",
                name: "attract"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/214/",
                name: "sleep-talk"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/216/",
                name: "return"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/218/",
                name: "frustration"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/219/",
                name: "safeguard"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/225/",
                name: "dragon-breath"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/231/",
                name: "iron-tail"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/237/",
                name: "hidden-power"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/241/",
                name: "sunny-day"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 34,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 34,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 34,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 34,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 49,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 49,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 39,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 39,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 39,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 39,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 49,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 49,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 49,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 50,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 50,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/245/",
                name: "extreme-speed"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/4/",
                        name: "crystal"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/3/",
                        name: "gold-silver"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/249/",
                name: "rock-smash"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/257/",
                name: "heat-wave"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/261/",
                name: "will-o-wisp"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/263/",
                name: "facade"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/270/",
                name: "helping-hand"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/290/",
                name: "secret-power"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/315/",
                name: "overheat"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/316/",
                name: "odor-sleuth"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/13/",
                        name: "xd"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/12/",
                        name: "colosseum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/7/",
                        name: "firered-leafgreen"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/6/",
                        name: "emerald"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/5/",
                        name: "ruby-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/332/",
                name: "aerial-ace"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/343/",
                name: "covet"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/363/",
                name: "natural-gift"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/406/",
                name: "dragon-pulse"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/416/",
                name: "giga-impact"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/422/",
                name: "thunder-fang"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/1/",
                        name: "level-up"
                    },
                    level_learned_at: 1,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/424/",
                name: "fire-fang"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/431/",
                name: "rock-climb"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/3/",
                        name: "tutor"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/442/",
                name: "iron-head"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/10/",
                        name: "heartgold-soulsilver"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/9/",
                        name: "platinum"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/8/",
                        name: "diamond-pearl"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/445/",
                name: "captivate"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/488/",
                name: "flame-charge"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/496/",
                name: "round"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/510/",
                name: "incinerate"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/514/",
                name: "retaliate"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/523/",
                name: "bulldoze"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/528/",
                name: "wild-charge"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/14/",
                        name: "black-2-white-2"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/11/",
                        name: "black-white"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/555/",
                name: "snarl"
            }
        },
        {
            version_group_details: [
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/17/",
                        name: "sun-moon"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/16/",
                        name: "omega-ruby-alpha-sapphire"
                    }
                },
                {
                    move_learn_method: {
                        url: "https://pokeapi.co/api/v2/move-learn-method/4/",
                        name: "machine"
                    },
                    level_learned_at: 0,
                    version_group: {
                        url: "https://pokeapi.co/api/v2/version-group/15/",
                        name: "x-y"
                    }
                }
            ],
            move: {
                url: "https://pokeapi.co/api/v2/move/590/",
                name: "confide"
            }
        }
    ],
    sprites: {
        back_female: null,
        back_shiny_female: null,
        back_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/59.png",
        front_female: null,
        front_shiny_female: null,
        back_shiny: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/59.png",
        front_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/59.png",
        front_shiny: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/59.png"
    },
    held_items: [
        {
            item: {
                url: "https://pokeapi.co/api/v2/item/129/",
                name: "rawst-berry"
            },
            version_details: [
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/22/",
                        name: "white-2"
                    },
                    rarity: 50
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/21/",
                        name: "black-2"
                    },
                    rarity: 50
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/18/",
                        name: "white"
                    },
                    rarity: 100
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/17/",
                        name: "black"
                    },
                    rarity: 100
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/16/",
                        name: "soulsilver"
                    },
                    rarity: 100
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/15/",
                        name: "heartgold"
                    },
                    rarity: 100
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/14/",
                        name: "platinum"
                    },
                    rarity: 100
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/13/",
                        name: "pearl"
                    },
                    rarity: 100
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/12/",
                        name: "diamond"
                    },
                    rarity: 100
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/11/",
                        name: "leafgreen"
                    },
                    rarity: 50
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/10/",
                        name: "firered"
                    },
                    rarity: 50
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/9/",
                        name: "emerald"
                    },
                    rarity: 100
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/8/",
                        name: "sapphire"
                    },
                    rarity: 100
                },
                {
                    version: {
                        url: "https://pokeapi.co/api/v2/version/7/",
                        name: "ruby"
                    },
                    rarity: 100
                }
            ]
        }
    ],
    location_area_encounters: "/api/v2/pokemon/59/encounters",
    height: 19,
    is_default: true,
    species: {
        url: "https://pokeapi.co/api/v2/pokemon-species/59/",
        name: "arcanine"
    },
    id: 59,
    order: 88,
    game_indices: [
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/22/",
                name: "white-2"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/21/",
                name: "black-2"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/18/",
                name: "white"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/17/",
                name: "black"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/16/",
                name: "soulsilver"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/15/",
                name: "heartgold"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/14/",
                name: "platinum"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/13/",
                name: "pearl"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/12/",
                name: "diamond"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/11/",
                name: "leafgreen"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/10/",
                name: "firered"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/9/",
                name: "emerald"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/8/",
                name: "sapphire"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/7/",
                name: "ruby"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/6/",
                name: "crystal"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/5/",
                name: "silver"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/4/",
                name: "gold"
            },
            game_index: 59
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/3/",
                name: "yellow"
            },
            game_index: 20
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/2/",
                name: "blue"
            },
            game_index: 20
        },
        {
            version: {
                url: "https://pokeapi.co/api/v2/version/1/",
                name: "red"
            },
            game_index: 20
        }
    ],
    base_experience: 194,
    types: [
        {
            slot: 1,
            type: {
                url: "https://pokeapi.co/api/v2/type/10/",
                name: "fire"
            }
        }
    ]
}

var pokemon = [arcanine.forms[0].name, arcanine.weight, arcanine.types[0].type.name, arcanine.moves[0].move.name]

var enteroString = `La suma entre un entero 5 y un string 'hola' es: ${funciones.Sumar(5, 'hola')}, su resta: ${funciones.Restar(5, 'hola')}, su multiplicacion: ${funciones.Multiplicar(5, 'hola')}, su division: ${funciones.Dividir(5, 'hola')} y su modulo: ${funciones.Modulo(5, 'hola')}`
var enteroBoolean = `La suma entre un entero 5 y un boolean true es: ${funciones.Sumar(5, true)}, su resta: ${funciones.Restar(5, true)}, su multiplicacion: ${funciones.Multiplicar(5, true)}, su division: ${funciones.Dividir(5, true)} y su modulo: ${funciones.Modulo(5, true)}`
var enteroUndefined = `La suma entre un entero 5 y una variable undefined es: ${funciones.Sumar(5, c)}, su resta: ${funciones.Restar(5, c)}, su multiplicacion: ${funciones.Multiplicar(5, c)}, su division: ${funciones.Dividir(5, c)} y su modulo: ${funciones.Modulo(5, c)}`
var enteroNull = `La suma entre un entero 5 y una variable null es: ${funciones.Sumar(5, null)}, su resta: ${funciones.Restar(5, null)}, su multiplicacion: ${funciones.Multiplicar(5, null)}, su division: ${funciones.Dividir(5, null)} y su modulo: ${funciones.Modulo(5, null)}`
var enteroJSON = `La suma entre un entero 5 y una variable json es: ${funciones.Sumar(5, arcanine)}, su resta: ${funciones.Restar(5, arcanine)}, su multiplicacion: ${funciones.Multiplicar(5, arcanine)}, su division: ${funciones.Dividir(5, arcanine)} y su modulo: ${funciones.Modulo(5, arcanine)}`
var enteroArray = `La suma entre un entero 5 y una arreglo es: ${funciones.Sumar(5, pokemon)}, su resta: ${funciones.Restar(5, pokemon)}, su multiplicacion: ${funciones.Multiplicar(5, pokemon)}, su division: ${funciones.Dividir(5, pokemon)} y su modulo: ${funciones.Modulo(5, pokemon)}`

console.log(enteroString)
console.log(enteroBoolean)
console.log(enteroNull)
console.log(enteroJSON)
console.log(enteroArray)

var stringString = `La suma entre un string 'hola' y otro string 'adios' es: ${funciones.Sumar('hola', 'adios')}, su resta: ${funciones.Restar('hola', 'adios')}, su multiplicacion: ${funciones.Multiplicar('hola', 'adios')}, su division: ${funciones.Dividir('hola', 'adios')} y su modulo: ${funciones.Modulo('hola', 'adios')}`
var stringBoolean = `La suma entre un string 'hola' y el boolean true es: ${funciones.Sumar('hola', true)}, su resta: ${funciones.Restar('hola', true)}, su multiplicacion: ${funciones.Multiplicar('hola', true)}, su division: ${funciones.Dividir('hola', true)} y su modulo: ${funciones.Modulo('hola', true)}`
var stringUndefined = `La suma entre un string 'hola' y una variable undefined es: ${funciones.Sumar('hola', c)}, su resta: ${funciones.Restar('hola', c)}, su multiplicacion: ${funciones.Multiplicar('hola', c)}, su division: ${funciones.Dividir('hola', c)} y su modulo: ${funciones.Modulo('hola', c)}`
var stringNull = `La suma entre un string 'hola' y una variable null es: ${funciones.Sumar('hola', null)}, su resta: ${funciones.Restar('hola', null)}, su multiplicacion: ${funciones.Multiplicar('hola', null)}, su division: ${funciones.Dividir('hola', null)} y su modulo: ${funciones.Modulo('hola', null)}`
var stringJSON = `La suma entre un string 'hola' y una variable json es: ${funciones.Sumar('hola', arcanine)}, su resta: ${funciones.Restar('hola', arcanine)}, su multiplicacion: ${funciones.Multiplicar('hola', arcanine)}, su division: ${funciones.Dividir('hola', arcanine)} y su modulo: ${funciones.Modulo('hola', arcanine)}`
var stringArray = `La suma entre un string 'hola' y un arreglo es: ${funciones.Sumar('hola', pokemon)}, su resta: ${funciones.Restar('hola', pokemon)}, su multiplicacion: ${funciones.Multiplicar('hola', pokemon)}, su division: ${funciones.Dividir('hola', pokemon)} y su modulo: ${funciones.Modulo('hola', pokemon)}`

console.log(stringString)
console.log(stringBoolean)
console.log(stringUndefined)
console.log(stringNull)
console.log(stringJSON)
console.log(stringArray)

var booleanBoolean = `La suma entre un boolean true y un boolea false es: ${funciones.Sumar(true, false)}, su resta: ${funciones.Restar(true, false)}, su multiplicacion: ${funciones.Multiplicar(true, false)}, su division: ${funciones.Dividir(true, false)} y su modulo: ${funciones.Modulo(true, false)}`
var booleanUndefined = `La suma entre un boolean true y una variable undefined es: ${funciones.Sumar(true, c)}, su resta: ${funciones.Restar(true, c)}, su multiplicacion: ${funciones.Multiplicar(true, c)}, su division: ${funciones.Dividir(true, c)} y su modulo: ${funciones.Modulo(true, c)}`
var booleanNull = `La suma entre un boolean true y una variable null es: ${funciones.Sumar(true, null)}, su resta: ${funciones.Restar(true, null)}, su multiplicacion: ${funciones.Multiplicar(true, null)}, su division: ${funciones.Dividir(true, null)} y su modulo: ${funciones.Modulo(true, null)}`
var booleanJSON = `La suma entre un boolean true y una variable null es: ${funciones.Sumar(true, arcanine)}, su resta: ${funciones.Restar(true, arcanine)}, su multiplicacion: ${funciones.Multiplicar(true, arcanine)}, su division: ${funciones.Dividir(true, arcanine)} y su modulo: ${funciones.Modulo(true, arcanine)}`
var booleanArray = `La suma entre un boolean true y una variable null es: ${funciones.Sumar(true, pokemon)}, su resta: ${funciones.Restar(true, pokemon)}, su multiplicacion: ${funciones.Multiplicar(true, pokemon)}, su division: ${funciones.Dividir(true, pokemon)} y su modulo: ${funciones.Modulo(true, pokemon)}`

console.log(booleanBoolean)
console.log(booleanUndefined)
console.log(booleanNull)
console.log(booleanJSON)
console.log(booleanArray)